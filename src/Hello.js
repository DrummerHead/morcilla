import React from 'react';

const Star = props => <span>*</span>

const Hello = props =>
  <div style={{backgroundColor: props.color}}>
    <Star />
    Hello {props.name}
    <Star />
  </div>

export default Hello;