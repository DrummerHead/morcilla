import React from 'react';
import Hello from './Hello'
import Counter from './Counter'

const valeria = false;

class App extends React.Component {
  render() {
    return (
      <div>
        <header>de la cabeza</header>
        <main>
          <Hello name='Valeria' color='blue' />
          <Hello name='Jorge' color='red' />
          <Counter />
        </main>
        <footer>de la pata</footer>
      </div>
    );
  }
}

export default App;
