import React from 'react'

class Counter extends React.Component {
  state = {
    number: 5
  }

  change = (doesIncrease) => {
    this.setState(prevState => {
      return {
        number: prevState.number +
          (doesIncrease ? 1 : -1)
      }
    })
  }

  increase = () => this.change(true)
  decrease = () => this.change(false)

  render() {
    return (
      <div className='counter'>
        <button onClick={this.increase}>+</button>
        {this.state.number}
        <button onClick={this.decrease}>-</button>
      </div>
    )
  }
}

export default Counter;